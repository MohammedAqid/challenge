# -*- coding: utf-8 -*-
"""author Mohammed Aqid Khatkhatay

Implement a string compression using python. 
For example, aaaabbbccddddddee would become a4b3c2d6e2. 
If the length of the string is not reduced, return the original string.
"""



def compress(a):
  count={}
  ans=''
  for i in a:  
    if i in count.keys(): #checking if alphabet is already there if yes increment the count else intialize it as 1
      count[i]+=1
    else:
      count[i]=1
  for i,j in count.items(): #extract the alphabet and count as a string and concat it to the ans
    ans+=str(i)+str(j)
  if len(a)<=len(ans): #check if length is reduced or no.
    return a
  else:
    return ans

print(compress("aaaabbbccddddddee")) 
print(compress('a'))
print(compress('aaabbbcccaaa'))
print(compress('aa'))
print(compress('aaab'))
"""
output
a4b3c2d6e2
a
a6b3c3
aa
aaab
"""
# Time Complexity is O(n) where n is the len(str). dict operations have a cost of O(1) in python 3.
